# Tarefa JS - Interface para Chamado de Suporte

Recebemos uma missão de desenvolver um projeto que se baseia numa chamado de suporte, a regra de negócio desse projeto é a seguinte:

- Temos um formulário contendo os seguintes campos:
	* Nome da pessoa que fez o chamado
	* E-mail
	* Telefone
	* Área do Chamado (Comercial, Financeiro, Técnico, Ouvidoria)
	* Assunto da Requisição
	* Detalhes
	* Data
	* Status (Aberto, Em Andamento, Concluido)

- Após o operador preencher esse formulário ele deverá salvar isso numa listagem abaixo do formulário que 
será atualizada automaticamente.
- Vale lembrar que campos selects como Área do Chamado e Status devem ser dinamicos, ou seja, ele carregará
os dados de um objeto, pois isso poderá mudar no futuro, sendo assim esses campos não podem ser estáticos.
- Em cada linha da listagem deverá conter 3 botões na qual representarão o Status, sendo assim o operador pode
aleterar o status de cada chamado através dos botões, sempre que isso ocorrer será necessário alterar esse item no objeto na qual ele está salvo.

## Dicas
 * Faça uma interface bonita e intuita pro usuário, pode usar e abusar do bootstrap
 * O array da Área do Chamado, será assim: var areasChamado = ["Comercial", "Fiannceiro", "Técnico", "Ouvidoria"]
 * Se baseando no array de Áreas, faça o array de Status.
 * Obejeto de Chamados
 	var chamados = [
 		{
 			"nome": "",
 			"email" : "",
	 		"telefone" : "",
	 		"area" : "",
	 		"assunto": "",
	 		"detalhes" : "",
	 		"data" : "",
	 		"status" : ""
	 	}
	];
 * Pense bem na solução para o problema, não se desespere, e use as melhores práticas possiveis
 * Pode usar qualquer coisa pra resolução e entrega da tarefa, (JS puro, jQuery, Angular...).
 * Atenção na estrutura de diretórios do projeto.

## Entrega
 * O prazo pra entrega é de 2 dias,se entregar em 1 tem bonus.
 * Você deverá criar clonar esse repositório para iniciar a tarefa
 * Deverá criar um branch dev
 * mudar para o branch dev e desenvolver a tarefa toda nesse branch específico
 * Comitar o resultado final no branch dev

Acho que é isso, sucesso na resolução da tarefa, e qualquer dúvida o Google e Stackoverflow estão aqui pra isso. ;)
